package net.oschina.app.ui;

import net.oschina.app.R;
import net.oschina.app.ui.fragments.SearchViewPagerFragment;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshAttacher;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;


public class SearchFragmentActivity extends BaseActivity{
	private PullToRefreshAttacher mPullToRefreshAttacher;
	private ActionBar mActionBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initActionBar();
		
		setContentView(R.layout.frame_main);

		FragmentTransaction t = this.getSupportFragmentManager()
				.beginTransaction();
		t = this.getSupportFragmentManager().beginTransaction();
		SearchViewPagerFragment nFrag = new SearchViewPagerFragment(this);
		t.replace(R.id.frame_main, nFrag);
		t.commit();
		mPullToRefreshAttacher = PullToRefreshAttacher.get(this);
	}
	private void initActionBar() {
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setTitle(R.string.head_title_news);
	}
	public PullToRefreshAttacher getPullToRefreshAttacher() {
		return mPullToRefreshAttacher;
	}
	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case android.R.id.home:
			finish();
			break;
		}
		return true;
	}
}
