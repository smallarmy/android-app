package net.oschina.app.ui.fragments;

import net.oschina.app.R;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

public class SoftwareLibViewPageFragment extends BaseMainViewPagerFragment {

	public SoftwareLibViewPageFragment(Context context, int position) {
		super(position);
		titleList.add(context.getString(R.string.frame_title_software_catalog));
		titleList.add(context.getString(R.string.frame_title_software_recommend));
		titleList.add(context.getString(R.string.frame_title_software_lastest));
		titleList.add(context.getString(R.string.frame_title_software_hot));
		titleList.add(context.getString(R.string.frame_title_software_china));
		fragmentList.add(new SoftwareCatalogListFragment(BaseSoftwareListFragment.HEAD_TAG_CATALOG, this));
		fragmentList.add(new BaseSoftwareListFragment(BaseSoftwareListFragment.HEAD_TAG_RECOMMEND, this));
		fragmentList.add(new BaseSoftwareListFragment(BaseSoftwareListFragment.HEAD_TAG_LASTEST, this));
		fragmentList.add(new BaseSoftwareListFragment(BaseSoftwareListFragment.HEAD_TAG_HOT, this));
		fragmentList.add(new BaseSoftwareListFragment(BaseSoftwareListFragment.HEAD_TAG_CHINA, this));
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case android.R.id.home:
			((BaseSoftwareListFragment)fragmentList.get(pager.getCurrentItem())).back();
			break;
		}
		return true;
	}
}
