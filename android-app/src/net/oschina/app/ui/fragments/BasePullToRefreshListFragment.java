package net.oschina.app.ui.fragments;

import net.oschina.app.ui.MainFragmentActivity;

import org.holoeverywhere.LayoutInflater;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshAttacher;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment Class
 */
public abstract class BasePullToRefreshListFragment extends BaseListFragment implements
		PullToRefreshAttacher.OnRefreshListener {

	protected PullToRefreshAttacher mPullToRefreshAttacher;
	
	public BasePullToRefreshListFragment(
			OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(baseListFragmentResumeListener);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mPullToRefreshAttacher = getFragmentActivity()
				.getPullToRefreshAttacher();
		((PullToRefreshLayout) view).setPullToRefreshAttacher(
				mPullToRefreshAttacher, this);
		return view;
	}

	
	public MainFragmentActivity getFragmentActivity() {
		return (MainFragmentActivity) getActivity();
	}

}