package net.oschina.app.ui.fragments;

public interface OnBaseListFragmentResumeListener {
	public void onBaseListFragmentResume(BaseListFragment baseListFragment);
}