package net.oschina.app.ui.fragments;

import net.oschina.app.R;
import net.oschina.app.bean.BlogList;
import net.oschina.app.bean.NewsList;
import net.oschina.app.common.UIHelper;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class NewsViewPagerFragment extends BaseMainViewPagerFragment {

	public NewsViewPagerFragment(Context context, int position) {
		super(position);
		titleList.add(context.getString(R.string.frame_title_news_lastest));
		titleList.add(context.getString(R.string.frame_title_news_blog));
		titleList.add(context.getString(R.string.frame_title_news_recommend));
		fragmentList.add(new BaseNewsListFragment(NewsList.CATALOG_ALL, this));
		fragmentList.add(new BaseBlogNewsListFragment(BlogList.CATALOG_LATEST, this));
		fragmentList.add(new BaseBlogNewsListFragment(BlogList.CATALOG_RECOMMEND, this));
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem item = menu.add(R.string.menu_title_search).setIcon(
				R.drawable.ic_action_search);
		MenuItemCompat.setShowAsAction(item,
				MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
	}

	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case android.R.id.home:
			getFragmentActivity().toggle();
			break;
		default:
			UIHelper.showSearch(getActivity());
		}
		return true;
	}
}
