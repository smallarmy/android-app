package net.oschina.app.ui.fragments;

import net.oschina.app.bean.NewsList;

public class LastestNewsListFragment extends BaseNewsListFragment {

	public LastestNewsListFragment(OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(NewsList.CATALOG_ALL, baseListFragmentResumeListener);
	}

}
