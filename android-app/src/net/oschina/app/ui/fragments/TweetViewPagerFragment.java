package net.oschina.app.ui.fragments;

import net.oschina.app.R;
import net.oschina.app.bean.TweetList;
import net.oschina.app.common.UIHelper;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class TweetViewPagerFragment extends BaseMainViewPagerFragment {

	public TweetViewPagerFragment(Context context, int position) {
		super(position);
		titleList.add(context.getString(R.string.frame_title_tweet_lastest));
		titleList.add(context.getString(R.string.frame_title_tweet_hot));
		titleList.add(context.getString(R.string.frame_title_tweet_my));
		fragmentList.add(new BaseTweetListFragment(TweetList.CATALOG_LASTEST,this));
		fragmentList.add(new BaseTweetListFragment(TweetList.CATALOG_HOT,this));
		fragmentList.add(new BaseTweetListFragment(TweetList.CATALOG_MY,this));
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem item = menu.add(R.string.menu_title_edit).setIcon(
				R.drawable.ic_action_edit);
		MenuItemCompat.setShowAsAction(item,
				MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
	}

	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case android.R.id.home:
			getFragmentActivity().toggle();
			break;
		default:
			UIHelper.showTweetPub(getActivity());
		}
		return true;
	}
}
