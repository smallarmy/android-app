package net.oschina.app.ui.fragments;

import net.oschina.app.bean.NewsList;

public class IntegrationNewsListFragment extends BaseNewsListFragment {

	public IntegrationNewsListFragment(OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(NewsList.CATALOG_INTEGRATION, baseListFragmentResumeListener);
	}

}
