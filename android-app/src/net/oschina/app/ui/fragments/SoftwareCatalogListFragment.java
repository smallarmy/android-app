package net.oschina.app.ui.fragments;

import net.oschina.app.R;
import net.oschina.app.common.UIHelper;

import org.holoeverywhere.LayoutInflater;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SoftwareCatalogListFragment extends BaseSoftwareListFragment {


	public SoftwareCatalogListFragment(int curHeadTag,
			OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(curHeadTag, baseListFragmentResumeListener);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_software_catalog,
				container, false);

		mlvSoftwareCatalog = (ListView) view.findViewById(R.id.listView1);
		TextView textView1 = (TextView) view.findViewById(R.id.textView1);
		mlvSoftwareCatalog.setEmptyView(textView1);

		mlvSoftwareTag = (ListView) view.findViewById(R.id.listView2);
		TextView textView2 = (TextView) view.findViewById(R.id.textView2);
		mlvSoftwareTag.setEmptyView(textView2);

		TextView textView3 = (TextView) view.findViewById(R.id.textView3);
		listView = (ListView) view.findViewById(R.id.listView3);
		listView.setEmptyView(textView3);

		list_footer = inflater.inflate(R.layout.listview_footer, null);
		list_foot_more = (TextView) list_footer
				.findViewById(R.id.listview_foot_more);
		list_foot_progress = (ProgressBar) list_footer
				.findViewById(R.id.listview_foot_progress);

		mPullToRefreshAttacher = getFragmentActivity()
				.getPullToRefreshAttacher();
		refreshLayout1 = (PullToRefreshLayout) view
				.findViewById(R.id.refresh_layout_1);
		refreshLayout1.setPullToRefreshAttacher(mPullToRefreshAttacher, this);
		refreshLayout2 = (PullToRefreshLayout) view
				.findViewById(R.id.refresh_layout_2);
		refreshLayout2.setPullToRefreshAttacher(mPullToRefreshAttacher, this);
		refreshLayout3 = (PullToRefreshLayout) view
				.findViewById(R.id.refresh_layout_3);
		refreshLayout3.setPullToRefreshAttacher(mPullToRefreshAttacher, this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initSoftwareCatalogListView();
		initSoftwareTagListView();
	}

	/**
	 * 返回事件
	 */
	protected void back() {
		switch (curScreen) {
		case SCREEN_SOFTWARE:
			scrollToScreen(SCREEN_TAG);
			break;
		case SCREEN_TAG:
			scrollToScreen(SCREEN_CATALOG);
			break;
		case SCREEN_CATALOG:
			getFragmentActivity().toggle();
			break;
		}
	}
	@Override
	public void onRefreshStarted(View view) {
		switch (curScreen) {
		case SCREEN_SOFTWARE:
			loadLvSoftwareTagData(curSecondSearchTag, 0, mSoftwareHandler,
					UIHelper.LISTVIEW_ACTION_REFRESH);
			break;
		case SCREEN_TAG:
			loadLvSoftwareCatalogData(curFirstSearchTag, mSoftwareTagHandler,
					UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG);
			break;
		case SCREEN_CATALOG:
			this.loadLvSoftwareCatalogData(0, mSoftwareCatalogHandler,
					UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG);
			break;
		}
	}

	@Override
	public void showList() {
		if (!isHasInit()) {
			this.loadLvSoftwareCatalogData(0, mSoftwareCatalogHandler,
					UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG);
			setHasInit(true);
		}
	}
}
