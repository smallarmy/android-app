package net.oschina.app.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import net.oschina.app.AppContext;
import net.oschina.app.AppException;
import net.oschina.app.R;
import net.oschina.app.adapter.ListViewMessageAdapter;
import net.oschina.app.bean.MessageList;
import net.oschina.app.bean.Messages;
import net.oschina.app.bean.Notice;
import net.oschina.app.bean.Result;
import net.oschina.app.common.StringUtils;
import net.oschina.app.common.UIHelper;
import net.oschina.app.widget.NewDataToast;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BaseMsgListFragment extends MainPullToRefreshListFragment {
	private ListViewMessageAdapter lvMsgAdapter;
	private List<Messages> lvMsgData = new ArrayList<Messages>();
	private int lvMsgSumData;
	private Handler lvMsgHandler;
	private boolean isClearNotice = false;
	private int curClearNoticeType = 0;
	public BaseMsgListFragment(
			OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(baseListFragmentResumeListener);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initMsgListView();
	}

	@Override
	public void onDestroyView() {
		lvMsgData.clear();
		super.onDestroyView();
	}

	/**
	 * 初始化留言列表
	 */
	private void initMsgListView() {
		lvMsgAdapter = new ListViewMessageAdapter(getActivity(), lvMsgData,
				R.layout.message_listitem);
		listView.addFooterView(list_footer);// 添加底部视图 必须在setAdapter前
		listView.setAdapter(lvMsgAdapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 点击头部、底部栏无效
				if (view == list_footer)
					return;

				Messages msg = null;
				// 判断是否是TextView
				if (view instanceof TextView) {
					msg = (Messages) view.getTag();
				} else {
					TextView tv = (TextView) view
							.findViewById(R.id.message_listitem_username);
					msg = (Messages) tv.getTag();
				}
				if (msg == null)
					return;

				// 跳转到留言详情
				UIHelper.showMessageDetail(view.getContext(),
						msg.getFriendId(), msg.getFriendName());
			}
		});
		listView.setOnScrollListener(new AbsListView.OnScrollListener() {
			public void onScrollStateChanged(AbsListView view, int scrollState) {

				// 数据为空--不用继续下面代码了
				if (lvMsgData.isEmpty())
					return;

				// 判断是否滚动到底部
				boolean scrollEnd = false;
				try {
					if (view.getPositionForView(list_footer) == view
							.getLastVisiblePosition())
						scrollEnd = true;
				} catch (Exception e) {
					scrollEnd = false;
				}

				int lvDataState = StringUtils.toInt(listView.getTag());
				if (scrollEnd && lvDataState == UIHelper.LISTVIEW_DATA_MORE) {
					listView.setTag(UIHelper.LISTVIEW_DATA_LOADING);
					list_foot_more.setText(R.string.load_ing);
					list_foot_progress.setVisibility(View.VISIBLE);
					// 当前pageIndex
					int pageIndex = lvMsgSumData / AppContext.PAGE_SIZE;
					loadLvMsgData(pageIndex, lvMsgHandler,
							UIHelper.LISTVIEW_ACTION_SCROLL);
				}
			}

			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
			}
		});
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 点击头部、底部栏无效
				if (view == list_footer)
					return false;

				Messages _msg = null;
				// 判断是否是TextView
				if (view instanceof TextView) {
					_msg = (Messages) view.getTag();
				} else {
					TextView tv = (TextView) view
							.findViewById(R.id.message_listitem_username);
					_msg = (Messages) tv.getTag();
				}
				if (_msg == null)
					return false;

				final Messages message = _msg;

				// 选择操作
				final Handler handler = new Handler() {
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							Result res = (Result) msg.obj;
							if (res.OK()) {
								lvMsgData.remove(message);
								lvMsgAdapter.notifyDataSetChanged();
							}
							UIHelper.ToastMessage(getActivity(),
									res.getErrorMessage());
						} else {
							((AppException) msg.obj).makeToast(getActivity());
						}
					}
				};
				Thread thread = new Thread() {
					public void run() {
						Message msg = new Message();
						try {
							Result res = appContext.delMessage(
									appContext.getLoginUid(),
									message.getFriendId());
							msg.what = 1;
							msg.obj = res;
						} catch (AppException e) {
							e.printStackTrace();
							msg.what = -1;
							msg.obj = e;
						}
						handler.sendMessage(msg);
					}
				};
				UIHelper.showMessageListOptionDialog(getActivity(), message,
						thread);
				return true;
			}
		});

		lvMsgHandler = this.getLvHandler(listView, lvMsgAdapter,
				list_foot_more, list_foot_progress, AppContext.PAGE_SIZE);
	}

	/**
	 * 线程加载留言数据
	 * 
	 * @param pageIndex
	 *            当前页数
	 * @param handler
	 * @param action
	 */
	private void loadLvMsgData(final int pageIndex, final Handler handler,
			final int action) {
		mPullToRefreshAttacher.setRefreshing(true);
		new Thread() {
			public void run() {
				Message msg = new Message();
				boolean isRefresh = false;
				if (action == UIHelper.LISTVIEW_ACTION_REFRESH
						|| action == UIHelper.LISTVIEW_ACTION_SCROLL)
					isRefresh = true;
				try {
					MessageList list = appContext.getMessageList(pageIndex,
							isRefresh);
					msg.what = list.getPageSize();
					msg.obj = list;
				} catch (AppException e) {
					e.printStackTrace();
					msg.what = -1;
					msg.obj = e;
				}
				msg.arg1 = action;
				msg.arg2 = UIHelper.LISTVIEW_DATATYPE_MESSAGE;
				handler.sendMessage(msg);
			}
		}.start();
	}

	/**
	 * listview数据处理
	 * 
	 * @param what
	 *            数量
	 * @param obj
	 *            数据
	 * @param objtype
	 *            数据类型
	 * @param actiontype
	 *            操作类型
	 * @return notice 通知信息
	 */
	private Notice handleLvData(int what, Object obj, int objtype,
			int actiontype) {
		Notice notice = null;
		switch (actiontype) {
		case UIHelper.LISTVIEW_ACTION_INIT:
		case UIHelper.LISTVIEW_ACTION_REFRESH:
		case UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG:
			int newdata = 0;// 新加载数据-只有刷新动作才会使用到
			switch (objtype) {
			case UIHelper.LISTVIEW_DATATYPE_MESSAGE:
				MessageList mlist = (MessageList) obj;
				notice = mlist.getNotice();
				lvMsgSumData = what;
				if (actiontype == UIHelper.LISTVIEW_ACTION_REFRESH) {
					if (lvMsgData.size() > 0) {
						for (Messages msg1 : mlist.getMessagelist()) {
							boolean b = false;
							for (Messages msg2 : lvMsgData) {
								if (msg1.getId() == msg2.getId()) {
									b = true;
									break;
								}
							}
							if (!b)
								newdata++;
						}
					} else {
						newdata = what;
					}
				}
				lvMsgData.clear();// 先清除原有数据
				lvMsgData.addAll(mlist.getMessagelist());
				break;
			}
			if (actiontype == UIHelper.LISTVIEW_ACTION_REFRESH && isVisible()) {
				// 提示新加载数据
				if (newdata > 0) {
					NewDataToast
							.makeText(
									getActivity(),
									getString(R.string.new_data_toast_message,
											newdata), appContext.isAppSound())
							.show();
				} else {
					NewDataToast.makeText(getActivity(),
							getString(R.string.new_data_toast_none), false)
							.show();
				}
			}
			break;
		case UIHelper.LISTVIEW_ACTION_SCROLL:
			switch (objtype) {
			case UIHelper.LISTVIEW_DATATYPE_MESSAGE:
				MessageList mlist = (MessageList) obj;
				notice = mlist.getNotice();
				lvMsgSumData += what;
				if (lvMsgData.size() > 0) {
					for (Messages msg1 : mlist.getMessagelist()) {
						boolean b = false;
						for (Messages msg2 : lvMsgData) {
							if (msg1.getId() == msg2.getId()) {
								b = true;
								break;
							}
						}
						if (!b)
							lvMsgData.add(msg1);
					}
				} else {
					lvMsgData.addAll(mlist.getMessagelist());
				}
				break;
			}
			break;
		}
		return notice;
	}

	/**
	 * 获取listview的初始化Handler
	 * 
	 * @param lv
	 * @param adapter
	 * @return
	 */
	protected Handler getLvHandler(final ListView lv,
			final BaseAdapter adapter, final TextView more,
			final ProgressBar progress, final int pageSize) {
		return new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what >= 0) {
					// listview数据处理
					Notice notice = handleLvData(msg.what, msg.obj, msg.arg2,
							msg.arg1);

					if (msg.what < pageSize) {
						lv.setTag(UIHelper.LISTVIEW_DATA_FULL);
						adapter.notifyDataSetChanged();
						more.setText(R.string.load_full);
					} else if (msg.what == pageSize) {
						lv.setTag(UIHelper.LISTVIEW_DATA_MORE);
						adapter.notifyDataSetChanged();
						more.setText(R.string.load_more);
					}
					// 发送通知广播
					if (notice != null) {
						UIHelper.sendBroadCast(lv.getContext(), notice);
					}
					// 是否清除通知信息
					 if (isClearNotice) {
						 ClearNotice(curClearNoticeType);
						 isClearNotice = false;// 重置
						 curClearNoticeType = 0;
					 }
				} else if (msg.what == -1) {
					// 有异常--显示加载出错 & 弹出错误消息
					lv.setTag(UIHelper.LISTVIEW_DATA_MORE);
					more.setText(R.string.load_error);
					((AppException) msg.obj).makeToast(getActivity());
				}
				if (adapter.getCount() == 0) {
					lv.setTag(UIHelper.LISTVIEW_DATA_EMPTY);
					more.setText(R.string.load_empty);
				}
				progress.setVisibility(ProgressBar.GONE);
				mPullToRefreshAttacher.setRefreshComplete();
				if (msg.arg1 == UIHelper.LISTVIEW_ACTION_REFRESH) {
					// getFragmentActivity().setActionBarSubtitle(getString(R.string.pull_to_refresh_update)
					// + new Date().toLocaleString());
					lv.setSelection(0);
				} else if (msg.arg1 == UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG) {
					lv.setSelection(0);
				}
			}
		};
	}

	@Override
	public void onRefreshStarted(View view) {
		isClearNotice = true;
		curClearNoticeType = Notice.TYPE_MESSAGE;
		// 刷新数据
		loadLvMsgData(0, lvMsgHandler, UIHelper.LISTVIEW_ACTION_REFRESH);
	}

	@Override
	public void showList() {
		if (!appContext.isLogin()) {
			UIHelper.showLoginDialog(getActivity());
			return;
		}
		if (!isHasInit()) {
			// 刷新数据
			loadLvMsgData(0, lvMsgHandler, UIHelper.LISTVIEW_ACTION_INIT);
			setHasInit(true);
		}

	}
	
	/**
	 * 通知信息处理
	 * 
	 * @param type
	 *            1:@我的信息 2:未读消息 3:评论个数 4:新粉丝个数
	 */
	private void ClearNotice(final int type) {
		final int uid = appContext.getLoginUid();
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == 1 && msg.obj != null) {
					Result res = (Result) msg.obj;
					if (res.OK() && res.getNotice() != null) {
						UIHelper.sendBroadCast(getActivity(), res.getNotice());
					}
				} else {
					((AppException) msg.obj).makeToast(getActivity());
				}
			}
		};
		new Thread() {
			public void run() {
				Message msg = new Message();
				try {
					Result res = appContext.noticeClear(uid, type);
					msg.what = 1;
					msg.obj = res;
				} catch (AppException e) {
					e.printStackTrace();
					msg.what = -1;
					msg.obj = e;
				}
				handler.sendMessage(msg);
			}
		}.start();
	}
}
