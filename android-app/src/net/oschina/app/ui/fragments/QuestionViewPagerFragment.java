package net.oschina.app.ui.fragments;

import net.oschina.app.R;
import net.oschina.app.bean.PostList;
import net.oschina.app.common.UIHelper;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class QuestionViewPagerFragment extends BaseMainViewPagerFragment {
	public QuestionViewPagerFragment(Context context,  int position) {
		super(position);
		titleList.add(context.getString(R.string.frame_title_question_ask));
		titleList.add(context.getString(R.string.frame_title_question_share));
		titleList.add(context.getString(R.string.frame_title_question_other));
		titleList.add(context.getString(R.string.frame_title_question_job));
		titleList.add(context.getString(R.string.frame_title_question_site));
		fragmentList.add(new BaseQuestionListFragment(PostList.CATALOG_ASK, this));
		fragmentList.add(new BaseQuestionListFragment(PostList.CATALOG_SHARE, this));
		fragmentList.add(new BaseQuestionListFragment(PostList.CATALOG_OTHER, this));
		fragmentList.add(new BaseQuestionListFragment(PostList.CATALOG_JOB, this));
		fragmentList.add(new BaseQuestionListFragment(PostList.CATALOG_SITE, this));
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem item = menu.add(R.string.menu_title_edit).setIcon(
				R.drawable.ic_action_edit);
		MenuItemCompat.setShowAsAction(item,
				MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
	}

	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case android.R.id.home:
			getFragmentActivity().toggle();
			break;
		default:
			UIHelper.showQuestionPub(getActivity());
		}
		return true;
	}
}
