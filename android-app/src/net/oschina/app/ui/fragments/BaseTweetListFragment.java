package net.oschina.app.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import net.oschina.app.AppConfig;
import net.oschina.app.AppContext;
import net.oschina.app.AppException;
import net.oschina.app.R;
import net.oschina.app.adapter.ListViewTweetAdapter;
import net.oschina.app.bean.Notice;
import net.oschina.app.bean.Result;
import net.oschina.app.bean.Tweet;
import net.oschina.app.bean.TweetList;
import net.oschina.app.common.StringUtils;
import net.oschina.app.common.UIHelper;
import net.oschina.app.ui.TweetPub;
import net.oschina.app.widget.NewDataToast;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BaseTweetListFragment extends MainPullToRefreshListFragment {
	private ListViewTweetAdapter lvTweetAdapter;
	private List<Tweet> lvTweetData = new ArrayList<Tweet>();
	private int lvTweetSumData;
	private Handler lvTweetHandler;
	private int curTweetCatalog;
	private TweetReceiver tweetReceiver;// 动弹发布接收器

	public BaseTweetListFragment(int curTweetCatalog,
			OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(baseListFragmentResumeListener);
		this.curTweetCatalog = curTweetCatalog;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initTweetListView();
		// 注册广播接收器
		tweetReceiver = new TweetReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction("net.oschina.app.action.APP_TWEETPUB");
		getActivity().registerReceiver(tweetReceiver, filter);
	}
	
	@Override
	public void onDestroyView() {
		getActivity().unregisterReceiver(tweetReceiver);
		lvTweetData.clear();
		super.onDestroyView();
	}
	/**
	 * 初始化动弹列表
	 */
	private void initTweetListView() {
		lvTweetAdapter = new ListViewTweetAdapter(getActivity(), lvTweetData,
				R.layout.tweet_listitem);
		listView.addFooterView(list_footer);// 添加底部视图 必须在setAdapter前
		listView.setAdapter(lvTweetAdapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				// 点击头部、底部栏无效
				if (view == list_footer)
					return;

				Tweet tweet = null;
				// 判断是否是TextView
				if (view instanceof TextView) {
					tweet = (Tweet) view.getTag();
				} else {
					TextView tv = (TextView) view
							.findViewById(R.id.tweet_listitem_username);
					tweet = (Tweet) tv.getTag();
				}
				if (tweet == null)
					return;
				// 跳转到动弹详情&评论页面
				UIHelper.showTweetDetail(view.getContext(), tweet.getId());
			}
		});
		listView.setOnScrollListener(new AbsListView.OnScrollListener() {
			public void onScrollStateChanged(AbsListView view, int scrollState) {

				// 数据为空--不用继续下面代码了
				if (lvTweetData.isEmpty())
					return;

				// 判断是否滚动到底部
				boolean scrollEnd = false;
				try {
					if (view.getPositionForView(list_footer) == view
							.getLastVisiblePosition())
						scrollEnd = true;
				} catch (Exception e) {
					scrollEnd = false;
				}

				int lvDataState = StringUtils.toInt(listView.getTag());
				if (scrollEnd && lvDataState == UIHelper.LISTVIEW_DATA_MORE) {
					listView.setTag(UIHelper.LISTVIEW_DATA_LOADING);
					list_foot_more.setText(R.string.load_ing);
					list_foot_progress.setVisibility(View.VISIBLE);
					// 当前pageIndex
					int pageIndex = lvTweetSumData / AppContext.PAGE_SIZE;
					loadLvTweetData(curTweetCatalog, pageIndex, lvTweetHandler,
							UIHelper.LISTVIEW_ACTION_SCROLL);
				}
			}

			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
			}
		});
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 点击头部、底部栏无效
				if (view == list_footer)
					return false;

				Tweet _tweet = null;
				// 判断是否是TextView
				if (view instanceof TextView) {
					_tweet = (Tweet) view.getTag();
				} else {
					TextView tv = (TextView) view
							.findViewById(R.id.tweet_listitem_username);
					_tweet = (Tweet) tv.getTag();
				}
				if (_tweet == null)
					return false;

				final Tweet tweet = _tweet;

				// 删除操作
				// if(appContext.getLoginUid() == tweet.getAuthorId()) {
				final Handler handler = new Handler() {
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							Result res = (Result) msg.obj;
							if (res.OK()) {
								lvTweetData.remove(tweet);
								lvTweetAdapter.notifyDataSetChanged();
							}
							UIHelper.ToastMessage(getActivity(),
									res.getErrorMessage());
						} else {
							((AppException) msg.obj).makeToast(getActivity());
						}
					}
				};
				Thread thread = new Thread() {
					public void run() {
						Message msg = new Message();
						try {
							Result res = appContext.delTweet(
									appContext.getLoginUid(), tweet.getId());
							msg.what = 1;
							msg.obj = res;
						} catch (AppException e) {
							e.printStackTrace();
							msg.what = -1;
							msg.obj = e;
						}
						handler.sendMessage(msg);
					}
				};
				UIHelper.showTweetOptionDialog(getActivity(), thread);
				// } else {
				// UIHelper.showTweetOptionDialog(Main.this, null);
				// }
				return true;
			}
		});
		lvTweetHandler = this.getLvHandler(listView, lvTweetAdapter,
				list_foot_more, list_foot_progress, AppContext.PAGE_SIZE);
	}

	/**
	 * 线程加载动弹数据
	 * 
	 * @param catalog
	 *            -1 热门，0 最新，大于0 某用户的动弹(uid)
	 * @param pageIndex
	 *            当前页数
	 * @param handler
	 *            处理器
	 * @param action
	 *            动作标识
	 */
	private void loadLvTweetData(final int catalog, final int pageIndex,
			final Handler handler, final int action) {
		mPullToRefreshAttacher.setRefreshing(true);
		new Thread() {
			public void run() {
				Message msg = new Message();
				boolean isRefresh = false;
				if (action == UIHelper.LISTVIEW_ACTION_REFRESH
						|| action == UIHelper.LISTVIEW_ACTION_SCROLL)
					isRefresh = true;
				try {
					TweetList list = appContext.getTweetList(catalog,
							pageIndex, isRefresh);
					msg.what = list.getPageSize();
					msg.obj = list;
				} catch (AppException e) {
					e.printStackTrace();
					msg.what = -1;
					msg.obj = e;
				}
				msg.arg1 = action;
				msg.arg2 = UIHelper.LISTVIEW_DATATYPE_TWEET;
				if (curTweetCatalog == catalog)
					handler.sendMessage(msg);
			}
		}.start();
	}

	/**
	 * listview数据处理
	 * 
	 * @param what
	 *            数量
	 * @param obj
	 *            数据
	 * @param objtype
	 *            数据类型
	 * @param actiontype
	 *            操作类型
	 * @return notice 通知信息
	 */
	private Notice handleLvData(int what, Object obj, int objtype,
			int actiontype) {
		Notice notice = null;
		switch (actiontype) {
		case UIHelper.LISTVIEW_ACTION_INIT:
		case UIHelper.LISTVIEW_ACTION_REFRESH:
		case UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG:
			int newdata = 0;// 新加载数据-只有刷新动作才会使用到
			switch (objtype) {
			case UIHelper.LISTVIEW_DATATYPE_TWEET:
				TweetList tlist = (TweetList) obj;
				notice = tlist.getNotice();
				lvTweetSumData = what;
				if (actiontype == UIHelper.LISTVIEW_ACTION_REFRESH) {
					if (lvTweetData.size() > 0) {
						for (Tweet tweet1 : tlist.getTweetlist()) {
							boolean b = false;
							for (Tweet tweet2 : lvTweetData) {
								if (tweet1.getId() == tweet2.getId()) {
									b = true;
									break;
								}
							}
							if (!b)
								newdata++;
						}
					} else {
						newdata = what;
					}
				}
				lvTweetData.clear();// 先清除原有数据
				lvTweetData.addAll(tlist.getTweetlist());
				break;
			}
			if (actiontype == UIHelper.LISTVIEW_ACTION_REFRESH && isVisible()) {
				// 提示新加载数据
				if (newdata > 0) {
					NewDataToast
							.makeText(
									getActivity(),
									getString(R.string.new_data_toast_message,
											newdata), appContext.isAppSound())
							.show();
				} else {
					NewDataToast.makeText(getActivity(),
							getString(R.string.new_data_toast_none), false)
							.show();
				}
			}
			break;
		case UIHelper.LISTVIEW_ACTION_SCROLL:
			switch (objtype) {
			case UIHelper.LISTVIEW_DATATYPE_TWEET:
				TweetList tlist = (TweetList) obj;
				notice = tlist.getNotice();
				lvTweetSumData += what;
				if (lvTweetData.size() > 0) {
					for (Tweet tweet1 : tlist.getTweetlist()) {
						boolean b = false;
						for (Tweet tweet2 : lvTweetData) {
							if (tweet1.getId() == tweet2.getId()) {
								b = true;
								break;
							}
						}
						if (!b)
							lvTweetData.add(tweet1);
					}
				} else {
					lvTweetData.addAll(tlist.getTweetlist());
				}
				break;
			}
			break;
		}
		return notice;
	}

	/**
	 * 获取listview的初始化Handler
	 * 
	 * @param lv
	 * @param adapter
	 * @return
	 */
	protected Handler getLvHandler(final ListView lv,
			final BaseAdapter adapter, final TextView more,
			final ProgressBar progress, final int pageSize) {
		return new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what >= 0) {
					// listview数据处理
					Notice notice = handleLvData(msg.what, msg.obj, msg.arg2,
							msg.arg1);

					if (msg.what < pageSize || (msg.what == pageSize && curTweetCatalog == TweetList.CATALOG_HOT)) {
						lv.setTag(UIHelper.LISTVIEW_DATA_FULL);
						adapter.notifyDataSetChanged();
						more.setText(R.string.load_full);
					} else if (msg.what == pageSize) {
						lv.setTag(UIHelper.LISTVIEW_DATA_MORE);
						adapter.notifyDataSetChanged();
						more.setText(R.string.load_more);
					}
					// 发送通知广播
					if (notice != null) {
						UIHelper.sendBroadCast(lv.getContext(), notice);
					}
				} else if (msg.what == -1) {
					// 有异常--显示加载出错 & 弹出错误消息
					lv.setTag(UIHelper.LISTVIEW_DATA_MORE);
					more.setText(R.string.load_error);
					((AppException) msg.obj).makeToast(getActivity());
				}
				if (adapter.getCount() == 0) {
					lv.setTag(UIHelper.LISTVIEW_DATA_EMPTY);
					more.setText(R.string.load_empty);
				}
				progress.setVisibility(ProgressBar.GONE);
				mPullToRefreshAttacher.setRefreshComplete();
				if (msg.arg1 == UIHelper.LISTVIEW_ACTION_REFRESH) {
					// getFragmentActivity().setActionBarSubtitle(getString(R.string.pull_to_refresh_update)
					// + new Date().toLocaleString());
					lv.setSelection(0);
				} else if (msg.arg1 == UIHelper.LISTVIEW_ACTION_CHANGE_CATALOG) {
					lv.setSelection(0);
				}
			}
		};
	}

	@Override
	public void onRefreshStarted(View view) {
		if (curTweetCatalog == TweetList.CATALOG_MY) {
			// 判断登录
			int uid = appContext.getLoginUid();
			if (uid == 0) {
				UIHelper.showLoginDialog(getActivity());
				return;
			}
			curTweetCatalog = uid;
		}
		loadLvTweetData(curTweetCatalog, 0, lvTweetHandler,
				UIHelper.LISTVIEW_ACTION_REFRESH);
	}

	@Override
	public void showList() {
		if (curTweetCatalog == TweetList.CATALOG_MY) {
			// 判断登录
			int uid = appContext.getLoginUid();
			if (uid == 0) {
				UIHelper.showLoginDialog(getActivity());
				return;
			}
			curTweetCatalog = uid;
		}
		if(!isHasInit()) {
			loadLvTweetData(curTweetCatalog, 0, lvTweetHandler,
					UIHelper.LISTVIEW_ACTION_INIT);
			setHasInit(true);
		}
	}
	
	public class TweetReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(final Context context, Intent intent) {
			int what = intent.getIntExtra("MSG_WHAT", 0);
			if (what == 1) {
				Result res = (Result) intent.getSerializableExtra("RESULT");
				UIHelper.ToastMessage(context, res.getErrorMessage(), 1000);
				if (res.OK()) {
					// 发送通知广播
					if (res.getNotice() != null) {
						UIHelper.sendBroadCast(context, res.getNotice());
					}
					// 发完动弹后-刷新最新动弹、我的动弹
					if(curTweetCatalog != TweetList.CATALOG_HOT)
						loadLvTweetData(curTweetCatalog, 0, lvTweetHandler,UIHelper.LISTVIEW_ACTION_REFRESH);
				}
			} else {
				final Tweet tweet = (Tweet) intent
						.getSerializableExtra("TWEET");
				final Handler handler = new Handler() {
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							Result res = (Result) msg.obj;
							UIHelper.ToastMessage(context,res.getErrorMessage(), 1000);
							if (res.OK()) {
								// 发送通知广播
								if (res.getNotice() != null) {
									UIHelper.sendBroadCast(context,res.getNotice());
								}
								// 发完动弹后-刷新最新动弹、我的动弹
								if(curTweetCatalog != TweetList.CATALOG_HOT)
									loadLvTweetData(curTweetCatalog, 0,lvTweetHandler,UIHelper.LISTVIEW_ACTION_REFRESH);
								if (TweetPub.mContext != null) {
									// 清除动弹保存的临时编辑内容
									appContext.removeProperty(AppConfig.TEMP_TWEET,AppConfig.TEMP_TWEET_IMAGE);
									((Activity) TweetPub.mContext).finish();
								}
							}
						} else {
							((AppException) msg.obj).makeToast(context);
							if (TweetPub.mContext != null&&TweetPub.mMessage != null)
								TweetPub.mMessage.setVisibility(View.GONE);
						}
					}
				};
				Thread thread = new Thread() {
					public void run() {
						Message msg = new Message();
						try {
							Result res = appContext.pubTweet(tweet);
							msg.what = 1;
							msg.obj = res;
						} catch (AppException e) {
							e.printStackTrace();
							msg.what = -1;
							msg.obj = e;
						}
						handler.sendMessage(msg);
					}
				};
				if (TweetPub.mContext != null)
					UIHelper.showResendTweetDialog(TweetPub.mContext, thread);
				else
					UIHelper.showResendTweetDialog(context, thread);
			}
		}
	}
}
