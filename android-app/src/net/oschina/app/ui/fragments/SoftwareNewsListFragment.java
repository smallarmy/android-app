package net.oschina.app.ui.fragments;

import net.oschina.app.bean.NewsList;

public class SoftwareNewsListFragment extends BaseNewsListFragment {

	public SoftwareNewsListFragment(OnBaseListFragmentResumeListener baseListFragmentResumeListener) {
		super(NewsList.CATALOG_SOFTWARE, baseListFragmentResumeListener);
	}

}
