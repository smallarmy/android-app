package net.oschina.app.ui.fragments;

import net.oschina.app.ui.MainFragmentActivity;
import android.view.MenuItem;

public class BaseMainViewPagerFragment extends BaseViewPagerFragment<MainPullToRefreshListFragment>{
	protected int position;

	public BaseMainViewPagerFragment(int position) {
		this.position = position;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case android.R.id.home:
			getFragmentActivity().toggle();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}
	
	public MainFragmentActivity getFragmentActivity() {
		return (MainFragmentActivity) getActivity();
	}
}
